<?php

//ESCOPO
$host_smtp = 'email-ssl.com.br';
$host_port = '465';
$email_envio = 'igor@layoutnet.com.br'; //Inserir Login do E-mail
$email_pass = ''; //Inserir Senha

//Váriveis de Formulário
$nome = !empty($_POST['nome']) ? filter_var($_POST['nome'], FILTER_SANITIZE_STRING) : null;
$email = !empty($_POST['email']) ? filter_var($_POST['email'], FILTER_SANITIZE_STRING) : null;
$mensagem = !empty($_POST['mensagem']) ? filter_var($_POST['mensagem'], FILTER_SANITIZE_STRING) : null;


date_default_timezone_set('America/Sao_Paulo');

$date = date('d/m/Y');
$hour = date('H:i');

# Inclui o arquivo class.phpmailer.php localizado na pasta phpmailer
require_once("PHPMailer/class.phpmailer.php");
require_once("PHPMailer/class.smtp.php");

# Inicia a classe PHPMailer
$mail = new PHPMailer();

# Define os dados do servidor e tipo de conexão
$mail->IsSMTP(); // Define que a mensagem será SMTP
$mail->Host = $host_smtp; # Endereço do servidor SMTP
$mail->Port = $host_port; // Porta TCP para a conexão
$mail->SMTPAutoTLS = false; // Utiliza TLS Automaticamente se disponível
$mail->SMTPAuth = true; # Usar autenticação SMTP - Sim
$mail->Username = $email_envio; # Usuário de e-mail
$mail->Password = $email_pass; // # Senha do usuário de e-mail
$mail->SMTPKeepAlive = true;
$mail->Timeout = 9000;

# Define o remetente (você)
$mail->From = "igor@layoutnet.com.br"; # Seu e-mail
$mail->FromName = "Contato - SBR"; // Seu nome

# Limpa os destinatários e os anexos
$mail->ClearAllRecipients();
$mail->ClearAttachments();


# Define os destinatário(s)
$mail->AddAddress("igor@layoutnet.com.br");


# Define os dados técnicos da Mensagem
$mail->IsHTML(true); # Define que o e-mail será enviado como HTML
$mail->CharSet = 'utf-8'; # Charset da mensagem (opcional)
$mail->SMTPDebug = 0; #Ativar Caso queira debugar o Envio de e-mail
$mail->SMTPSecure = "ssl";


# Define a mensagem (Texto e Assunto)
$mail->Subject = "Contato - Site SBR"; # Assunto da mensagem
$mail->AltBody = "Este é o corpo da mensagem de teste, somente Texto! \r\n :)";


$msg = '<table width="650" cellspacing="0" cellpadding="0" border="0" bordercolor="#fff" align="center">
			<tr style="background:#ff8a00;">
				<td align="center"> <br> <br> <img style="width: 50%;" src="http://localhost/sbr/assets/images/logo-footer-sbr.png" alt="logo"> <br> <br> <br></td>
			</tr>
			<tr style="background: #1c3a59">
				<td>
				<br>
				<br>
				<b><p style="color: #fff; text-align: left; font-family: Roboto Condensed, sans-serif; margin-left: 50px"> ' . $nome . ', preencheu o formulário no site, com as credenciais abaixo:</p></b>
				<b><p style="color: #fff; text-align: left; font-family: Roboto Condensed, sans-serif; margin-left: 50px">E-mail: ' . $email . '</p></b>
				<b><p style="color: #fff; text-align: left; font-family: Roboto Condensed, sans-serif; margin-left: 50px">Mensagem: ' . $mensagem . '</p></b>
				<b><p style="color: #fff; text-align: left; font-family: Roboto Condensed, sans-serif; margin-left: 50px">Enviado em: ' . $date . ' , ' . $hour . '</p></b>
				<br>
				<br>
				</td>
			</tr>
		</table>';

$mail->Body = $msg;

# Envia o e-mail
$enviado = $mail->Send();


if ($enviado) {
	echo "<script>alert('Sua Mensagem Foi Enviada com Sucesso');</script>";
	echo "<meta HTTP-EQUIV='refresh' CONTENT='1;URL=../'>";
	exit;
} else {
	echo "<script>alert('Sua mensagem não pode ser enviada, tente entrar em contato pelo telefone: (55-11) 2118-8311');</script>";
	echo "<meta HTTP-EQUIV='refresh' CONTENT='1;URL=../'>";
}
