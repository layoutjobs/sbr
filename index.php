<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="LayoutNet - Rua Europa 1303 - Jd. Celani - Salto/SP">
    <meta name="description" content="Especializada na fabricação e avaliação de desgaste de equipamentos, utiliza tecnologias de revestimentos especiais, soldagem, placas antidesgastes e metalização.">
    <meta name="keywords" content="metalização, soldagem, placas antidesgaste, avaliação de equipamentos, manutenção de equipamentos cromo duro, asperção térmica, fabricação, revestimentos especiais">
    <title>SBRevestimentos</title>

    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/mystyles.css">

    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/7bc0885a91.js"></script>
</head>

<body>
    <header>
        <div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar">
            <nav class="uk-navbar-container" uk-navbar>
                <div class="uk-navbar-left">
                    <a class="uk-navbar-item uk-logo uk-padding-small" href="#"><img src="assets/images/logo-sbr.png" alt="logo-sbr"></a>
                </div>
                <div class="uk-navbar-right uk-visible@l">
                    <ul class="uk-navbar-nav" uk-scrollspy-nav="closest: li; scroll: true; offset: 120; cls: uk-active">
                        <li><a href="#quem-somos">Quem Somos</a></li>
                        <li><a href="#produtos">Produtos</a></li>
                        <li><a href="#servicos">Serviços</a></li>
                        <li><a href="#servicos-de-aspersao">Serviços de Aspersão</a></li>
                        <li><a href="assets/pdfs/sbr-revestimentos.pdf" target="_blank">Catálogo</a></li>
                        <li><a href="#contato">Contato</a></li>
                        <li><a href="https://api.whatsapp.com/send?phone=5519996564766" target="_blank"><i class="fab fa-whatsapp"></i> &nbsp; 19 9 9656 4766</a></li>
                    </ul>
                </div>
                <!-- Canvas -->
                <a class="uk-navbar-toggle uk-hidden@l uk-position-right" uk-toggle="target: #offcanvas-nav-primary"><span class="off-canvas"><i class="fas fa-bars"></i> Menu</span></a>
            </nav>
        </div>
    </header>

    <main>
        <section id="slider-intro">
            <?php require('pages/intro.php') ?>
        </section>
        <section id="quem-somos">
            <?php require('pages/quem-somos.php') ?>
        </section>
        <section id="produtos">
            <?php require('pages/produtos.php') ?>
        </section>
        <section id="servicos">
            <?php require('pages/servicos.php') ?>
        </section>
    </main>



    <footer id="contato">
        <div class="uk-container uk-padding">
            <div class="uk-text-left" uk-grid>
                <div class="uk-width-1-2@m">
                    <div class="footer-intro">
                        <i class="far fa-comment-dots"></i> <span>Fale Conosco</span>
                        <p>Para mais informações ou detalhes sobre produtos e serviços, entre em contato. </p>
                        <p>Preencha o formulário abaixo, ou faça contato através de nosso Whatsapp ou celular. </p>
                        <p>Estamos a disposição para atendê-lo.</p>
                    </div>
                </div>
                <div class="uk-width-1-2@m">
                    <div class="logo-footer uk-text-right@l uk-padding-small">
                        <img src="assets/images/logo-footer-sbr.png" alt="logo-footer-sbr">
                    </div>
                </div>
            </div>

            <div class="block-form">
                <form class="uk-grid-small formphp" method="POST" action="api/enviar.php" uk-grid>
                    <div class="uk-width-1-2@s">
                        <input class="uk-input" type="text" name="nome" placeholder="NOME" required>
                    </div>
                    <div class="uk-width-1-2@s">
                        <input class="uk-input" type="email" name="email" placeholder="E-MAIL" required>
                    </div>
                    <div class="uk-width-1-1@s">
                        <div class="uk-margin">
                            <textarea class="uk-textarea" rows="5" name="mensagem" placeholder="DESCRIÇÃO" required></textarea>
                        </div>
                    </div>
                    <div class="uk-width-1-1@s">
                        <label class="nao-aparece">Se você não é um robô, deixe em branco.</label>
                        <input type="text" class="nao-aparece" name="leaveblank">
                    </div>
                    <div class="uk-width-1-1@s">
                        <label class="nao-aparece">Se você não é um robô, não mude este campo.</label>
                        <input type="text" class="nao-aparece" name="dontchange" value="http://">
                    </div>
                    <div class="uk-width-1-1@s">
                        <div class="btn-area uk-text-center">
                            <button class="uk-button uk-button-default" type="submit">ENVIAR</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="icons-contact">
                <div class="uk-grid-large uk-child-width-expand@l" uk-grid>
                    <div>
                        <div class="uk-card">
                            <table>
                                <tbody>
                                    <td><i class="fas fa-phone"></i></td>
                                    <td><a class="phone" href="https://api.whatsapp.com/send?phone=5519996564766" target="_blank">(19) 9 9625 4766 (Vivo)</a> <br> <a class="phone" href="tel:+5511987510056">(11) 9 8751 0056 (TIM)</a></td>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <table>
                                <tbody>
                                    <td><i class="fas fa-envelope"></i></td>
                                    <td>
                                        <a href="mailto:contato@sbrevestimentos.com.br">contato@sbrevestimentos.com.br</a>
                                    </td>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <table>
                                <tbody>
                                    <td><i class="fas fa-map-marker-alt"></i></td>
                                    <td>
                                        <p>Escrit.: (19) 3816 4358 - Salto /SP <br> Rua Asturias, 642 - B. Sol D'Icaraí</p>
                                    </td>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <div class="map-area">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3667.6973985287455!2d-47.32245188441367!3d-23.181241853550937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf5245a1d0c039%3A0x52b69f838795cc7f!2sR.%20Asturia%2C%20642%20-%20Jardim%20D&#39;icarai%2C%20Salto%20-%20SP!5e0!3m2!1spt-BR!2sbr!4v1572369799470!5m2!1spt-BR!2sbr" width="100%" height="750" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>

    <div class="copyright uk-padding-small">
        <h6 class="uk-text-center uk-margin-remove">TODOS OS DIREITOS RESERVADOS - SBRevestimentos &copy; <?php echo date("Y"); ?></h6>
    </div>

    <!-- Social -->
    <div class="sidebar-social">
        <ul>
            <li>
                <a href="" class="fb" target="_blank">
                    <i class="fab fa-facebook-f" aria-hidden="true"></i>
                </a>
            </li>
            <li>
                <a href="" class="linkedin" target="_blank">
                    <i class="fab fa-linkedin" aria-hidden="true"></i>
                </a>
            </li>
            <li>
                <a href="mailto:contato@sbrevestimentos" class="email" target="_blank">
                    <i class="fas fa-envelope" aria-hidden="true"></i>
                </a>
            </li>
            <li>
                <a href="https://api.whatsapp.com/send?phone=5519996564766" class="whatsapp" target="_blank">
                    <i class="fab fa-whatsapp" aria-hidden="true"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="overlay"></div>


    <!-- Canvas -->
    <div id="offcanvas-nav-primary" uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar uk-flex uk-flex-column" id="canvas-mobile">
            <div class="brand uk-text-center">
                <a class="uk-offcanvas-brand" href=""><img src="assets/images/logo-footer-sbr.png" alt="logo-sbr"></a>
            </div>
            <ul class="uk-nav uk-nav-primary uk-padding-small uk-nav-left uk-margin-auto-vertical" uk-scrollspy-nav="closest: li; scroll: true; offset: 120">
                <li class="offcanvas_li"><a href="#quem-somos">Quem Somos</a></li>
                <li><a href="#produtos">Produtos</a></li>
                <li><a href="#servicos">Serviços</a></li>
                <li><a href="#servicos-de-aspersao">Serviços de Aspersão</a></li>
                <li><a href="#contato">Contato</a></li>
                <li><a href="assets/pdfs/sbr-revestimentos.pdf" target="_blank">Catálogo</a></li>
                <li class="uk-nav-divider"></li>
                <li><a href="https://api.whatsapp.com/send?phone=5519996564766" target="_blank"><i class="fab fa-whatsapp"></i> 19 9 9656 4766</a></li>
            </ul>
        </div>
    </div>



    <!-- JS -->
    <script src="assets/js/main.js"></script>
</body>

</html>