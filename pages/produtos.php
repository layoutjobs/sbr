<div class="banner-area uk-padding">
    <div class="uk-container uk-visible@l">
        <div class="block-title">
            <h2>Produtos</h2>
        </div>
        <div class="intro-text">
            <p>TRABALHAMOS COM AS MELHORES MARCAS DE CONSUMÍVEIS DE SOLDA DO MERCADO NACIONAL </p>
        </div>
    </div>
</div>

<div class="block-darkblue uk-hidden@l">
    <div class="block-title">
        <h2>Produtos</h2>
        <hr />
    </div>
    <div class="intro-text">
        <p>TRABALHAMOS COM AS MELHORES MARCAS DE CONSUMÍVEIS DE SOLDA DO MERCADO NACIONAL </p>
    </div>
</div>

<div class="product-intro">
    <div class="uk-container uk-padding">
        <table class="uk-table uk-table-divider uk-visible@l">
            <tbody>
                <tr>
                    <td><i class="fas fa-circle"></i> Arames sólidos ferrosos e não ferrosos</td>
                    <td><i class="fas fa-circle"></i> Arames Tubulares</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Eletrodos revestidos</td>
                    <td><i class="fas fa-circle"></i> Equipamentos de matalização</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Insumos para solda e metalização</td>
                    <td><i class="fas fa-circle"></i> Máquina de solda</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Placas de desgaste</td>
                    <td><i class="fas fa-circle"></i> Varetas para soldagem tig e oxiacetileno</td>
                </tr>
            </tbody>
        </table>

        <table class="uk-table uk-table-divider uk-hidden@l">
            <tbody>
                <tr>
                    <td><i class="fas fa-circle"></i> Arames sólidos ferrosos e não ferrosos</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Arames Tubulares</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Eletrodos revestidos</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Equipamentos de matalização</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Equipamentos de matalização</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i>Máquina de solda</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Placas de desgaste</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Varetas soldagem tig e oxiacetileno</td>
                </tr>
            </tbody>
        </table>

        <div class="spacing-sm"></div>
        <p class="uk-text-center">A SBRevestimentos oferece a melhor qualidade em insumos e equipamentos para solda e revestimentos. A SBR representa as melhores marcas nacionais e mundiais dentro do setor de soldas e revestimentos. <strong>Escolha a marca abaixo e clique para visualizar:</strong></p>
        <div class="spacing-sm"></div>
        <ul uk-accordion>
            <li>
                <a class="uk-accordion-title" href="#">
                    <div uk-grid>
                        <div class="uk-width-1-2@m"><img src="assets/images/bohler-logo.png" alt="bohler-logo"></div>
                        <div class="divider"></div>
                        <div class="uk-width-1-3@m uk-visible@m flex">
                            <ul class="uk-list uk-list-bullet">
                                <li>Representante exclusivo</li>
                                <li>Insumos para solda</li>
                                <li>Varetas e arames</li>
                            </ul>
                        </div>
                    </div>
                </a>
                <div class="uk-accordion-content uk-padding-small">
                    <div class="download-pdf">
                        <table class="uk-table uk-table-divider uk-text-center uk-table-responsive">
                            <thead>
                                <tr>
                                    <th>Arquivo</th>
                                    <th>Nome</th>
                                    <th>Descrição</th>
                                    <th>Download</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><i class="far fa-file-pdf"></i></td>
                                    <td><strong>BOHLER</strong></td>
                                    <td>Consumíveis para Solda</td>
                                    <td><a href="assets/pdfs/bohler.pdf" target="_blank"><i class="fas fa-file-download"></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </li>
        </ul>
        <ul uk-accordion>
            <li>
                <a class="uk-accordion-title" href="#">
                    <div uk-grid>
                        <div class="uk-width-1-2@m"><img src="assets/images/logo-utp.png" alt="utp-logo"></div>
                        <div class="divider"></div>
                        <div class="uk-width-1-3@m uk-visible@m flex">
                            <ul class="uk-list uk-list-bullet">
                                <li>Representante exclusivo</li>
                                <li>Insumos para solda</li>
                                <li>Varetas e arames</li>
                            </ul>
                        </div>
                    </div>
                </a>
                <div class="uk-accordion-content uk-padding-small">
                    <div class="download-pdf">
                        <table class="uk-table uk-table-divider uk-text-center uk-table-responsive">
                            <thead>
                                <tr>
                                    <th>Arquivo</th>
                                    <th>Nome</th>
                                    <th>Descrição</th>
                                    <th>Download</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><i class="far fa-file-pdf"></i></td>
                                    <td><strong>UTP Maintenance</strong></td>
                                    <td>Manutenção e Cladding</td>
                                    <td><a href="assets/pdfs/utp-maintenance2014.pdf" target="_blank"><i class="fas fa-file-download"></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </li>
        </ul>
        <ul uk-accordion>
            <li>
                <a class="uk-accordion-title" href="#">
                    <div uk-grid>
                        <div class="uk-width-1-2@m"><img src="assets/images/logo-abicor.png" alt="abicor-logo"></div>
                        <div class="divider"></div>
                        <div class="uk-width-1-3@m uk-visible@m flex">
                            <ul class="uk-list uk-list-bullet">
                                <li>Tochas MIG/MAG</li>
                                <li>Tochas TIG</li>
                                <li>Tochas de Corte Plasma</li>
                            </ul>
                        </div>
                    </div>
                </a>
                <div class="uk-accordion-content uk-padding-small">
                    <div class="download-pdf">
                        <table class="uk-table uk-table-divider uk-text-center uk-table-responsive">
                            <thead>
                                <tr>
                                    <th>Arquivo</th>
                                    <th>Nome</th>
                                    <th>Descrição</th>
                                    <th>Download</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><i class="far fa-file-pdf"></i></td>
                                    <td><strong>Abicor</strong></td>
                                    <td>Catalogo de Acessórios</td>
                                    <td><a href="assets/pdfs/catalogo-de-acessorios-abicor.pdf" target="_blank"><i class="fas fa-file-download"></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </li>
        </ul>
        <ul uk-accordion>
            <li>
                <a class="uk-accordion-title" href="#">
                    <div uk-grid>
                        <div class="uk-width-1-2@m"><img src="assets/images/logo-aotai.png" alt="aotai-logo"></div>
                        <div class="divider"></div>
                        <div class="uk-width-1-3@m uk-visible@m flex">
                            <ul class="uk-list uk-list-bullet">
                                <li>Máquinas MMA</li>
                                <li>GTAW - Tecnologia TIG</li>
                                <li>Gmaw-MIG/MAG</li>
                            </ul>
                        </div>
                    </div>
                </a>
                <div class="uk-accordion-content uk-padding-small">
                    <div class="download-pdf">
                        <table class="uk-table uk-table-divider uk-text-center uk-table-responsive">
                            <thead>
                                <tr>
                                    <th>Arquivo</th>
                                    <th>Nome</th>
                                    <th>Descrição</th>
                                    <th>Download</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </li>
        </ul>
        <ul uk-accordion>
            <li>
                <a class="uk-accordion-title" href="#">
                    <div uk-grid>
                        <div class="uk-width-1-2@m"><img src="assets/images/logo-harris.png" alt="harris-logo"></div>
                        <div class="divider"></div>
                        <div class="uk-width-1-3@m uk-visible@m flex">
                            <ul class="uk-list uk-list-bullet harris">
                                <li>Varetas para metalização</li>
                                <li>Equipamentos metalização</li>
                                <li>Nano Tecnologia industrial</li>
                            </ul>
                        </div>
                    </div>
                </a>
                <div class="uk-accordion-content uk-padding-small">
                    <div class="download-pdf">
                        <table class="uk-table uk-table-divider uk-text-center uk-table-responsive">
                            <thead>
                                <tr>
                                    <th>Arquivo</th>
                                    <th>Nome</th>
                                    <th>Descrição</th>
                                    <th>Download</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><i class="far fa-file-pdf"></i></td>
                                    <td><strong>NANO MATERIAIS SHS</strong></td>
                                    <td>Nanotecnologia empregada em revestimento duro </td>
                                    <td><a href=""><i class="fas fa-file-download"></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </li>
        </ul>

        <div class="spacing"></div>
        <div class="products-sbr">
            <strong>
                <p class="uk-text-center">Produtos Exclusivos SBRevestimentos:</p>
            </strong>
            <ul uk-accordion>
                <li>
                    <a class="uk-accordion-title" href="#">
                        <div uk-grid>
                            <div class="uk-width-1-2@m"><img src="assets/images/produto-sbr.png" alt="produto-sbr"></div>
                            <div class="divider"></div>
                            <div class="uk-width-1-3@m uk-visible@m flex">
                                <ul class="uk-list uk-list-bullet">
                                    <li>Lanças de Oxiflame</li>
                                    <li>Blocos de Desgaste</li>
                                </ul>
                            </div>
                        </div>
                    </a>
                    <div class="uk-accordion-content">
                        <div class="download-pdf uk-text-center">
                            <table class="uk-table uk-table-divider uk-text-center uk-table-responsive">
                                <thead>
                                    <tr>
                                        <th>Arquivo</th>
                                        <th>Nome</th>
                                        <th>Descrição</th>
                                        <th>Download</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><i class="far fa-file-pdf"></i></td>
                                        <td><strong>SBR - Revestimentos</strong></td>
                                        <td>Soldagem de Revestimento</td>
                                        <td><a href="assets/pdfs/SBR-Revestimentos.pdf" target="_blank"><i class="fas fa-file-download"></i></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="block-atendimento">
            <div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin-large-top" uk-grid>
                <div>
                    <div class="block-orange">
                        <p>ATENDEMOS TODO O TERRITÓRIO NACIONAL.</p>
                    </div>
                </div>
                <div>
                    <div><img class="uk-width-expand" src="assets/images/atendimento-sbr.png" alt="atendimento"></div>
                </div>
            </div>
        </div>
    </div>
</div>