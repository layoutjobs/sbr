<div class="uk-container uk-padding">
    <div class="uk-child-width-expand@l" uk-grid>
        <div>
            <div class="uk-card">
                <div class="block-title">
                    <h3>Quem Somos</h3>
                </div>
                <div class="block-text">
                    <p>A SBR possui vasto conhecimento nos principais setores da indústria brasileira com foco específico para cada um deles, através da experiência e expertise da sua equipe.
                        A SBR desenvolve produtos e processos que atendem às exigências e certificações do mercado nacional. A equipe SBR está preparada para atender e solucionar os pontos críticos do processo. </p>
                </div>
            </div>
        </div>
        <div>
            <div class="uk-card">
                <div class="block-title">
                    <h3>Temos como Objetivo</h3>
                </div>
                <div class="block-topic">
                    <ul class="uk-list uk-list-bullet">
                        <li>Metalização, Soldas Especiais, Caldeiraria e Usinagem.</li>
                        <li>Projetos mediante as necessidades do cliente. </li>
                        <li>Comercializar consumíveis e máquinas de solda.</li>
                        <li>Comercializar consumíveis e máquinas de metalização.</li>
                    </ul>
                    <a href="assets/pdfs/sbr-revestimentos.pdf" target="_blank"><strong>CATÁLOGO VIRTUAL – Download. </strong></a>
                </div>
            </div>
        </div>
    </div>

    <div class="block-certified">
        <div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin-large-top" uk-grid>
            <div>
                <div class="block-orange">
                    <p>Certificação PETROBRAS para produtos e serviços.</p>
                </div>
            </div>
            <div>
                <div><img class="uk-width-expand" src="assets/images/certified.png" alt="certified"></div>
            </div>
        </div>
    </div>

    <div class="intro-carousel">
        <div class="spacing-lg"></div>
        <h3 class="uk-text-center uk-text-uppercase">Os engenheiros da SBRevestimentos estão capacitados para:</h3>

        <div class="slider-area">
            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="autoplay:true; autoplay-interval: 2000">

                <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-4@m uk-grid">
                    <li>
                        <div class="uk-panel uk-text-center">
                            <img src="assets/images/rounded-projeto.png" alt="rounded-projeto">
                            <div class="spacing"></div>
                            <div class="icon-area">
                                <img src="assets/images/chart-line-solid.svg" alt="chart">
                                <h3>Projeto</h3>
                                <p>Dispomos de recursos que nos permite avaliar o desgaste e propor uma melhor opção técnica para maior durabilidade dos equipamentos.</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="uk-panel uk-text-center">
                            <img src="assets/images/rounded-analise.png" alt="rounded-analise">
                            <div class="spacing"></div>
                            <div class="icon-area">
                                <img src="assets/images/cogs-solid.svg" alt="icon-analise">
                                <h3>Análise</h3>
                                <p>Apoiado por metodologias, realizamos análise dos pontos críticos dos equipamentos em operação, definindo as soluções para aumento na disponibilidade.</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="uk-panel uk-text-center">
                            <img src="assets/images/rounded-fabricacao.png" alt="rounded-fabricacao">
                            <div class="spacing"></div>
                            <div class="icon-area">
                                <img src="assets/images/tools-solid.svg" alt="icon-fabricacao">
                                <h3>Fabricação</h3>
                                <p>A SBRevestimentos oferece total infraestrutura para atender projetos de fabricação e ajustes em linhas e equipamentos de pequeno, médio e grande porte.</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="uk-panel uk-text-center">
                            <img src="assets/images/rounded-tecnologia.png" alt="rounded-tecnologia">
                            <div class="spacing"></div>
                            <div class="icon-area">
                                <img src="assets/images/codepen-brands.svg" alt="icon-tecnologia">
                                <h3>Tecnologia</h3>
                                <p>Soldagem com eletrodos especiais e arames tubulares. Metalização pelos processos de Flame Spray e HVOF Spray. Alta tecnologia em processos de soldagem.</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="uk-panel uk-text-center">
                            <img src="assets/images/rounded-revestimentos.png" alt="rounded-revestimento">
                            <div class="spacing"></div>
                            <div class="icon-area">
                                <img src="assets/images/cubes-solid.svg" alt="icon-tecnologia">
                                <h3>Revestimento</h3>
                                <p>Experiência e tecnologia para aplicação de revestimentos especiais com ligas a base de carboneto de tungstênio, cerâmicos e ligas metálicas.</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>