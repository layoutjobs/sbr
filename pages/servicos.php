<div class="banner-area-services uk-padding">
    <div class="uk-container uk-visible@l">
        <div class="block-title">
            <h2>Serviços</h2>
        </div>
        <div class="intro-text">
            <p>PROCESSOS COM QUALIDADE, AGILIDADE E TECNOLOGIA</p>
        </div>
    </div>
</div>

<div class="block-darkblue uk-hidden@l">
    <div class="block-title">
        <h2>Serviços</h2>
        <hr />
    </div>
    <div class="intro-text">
        <p>TRABALHAMOS COM AS MELHORES MARCAS DE CONSUMÍVEIS DE SOLDA DO MERCADO NACIONAL </p>
    </div>
</div>

<div class="intro-services">
    <div class="uk-container uk-padding">
        <p class="uk-text-center">A SBRevestimentos oferece serviços através de profissionais altamente treinados e qualificados para oferecer soluções com agilidade e total eficiência. A SBRevestimentos oferece o melhor custo x benefício do mercado, com know-how para atender projetos de pequeno, médio e grande porte. </p>

        <div class="spacing uk-visible@l"></div>
        <table class="uk-table uk-table-divider uk-visible@l">
            <tbody>
                <tr>
                    <td><i class="fas fa-circle"></i> Caldeiraria</td>
                    <td><i class="fas fa-circle"></i> Metalização</td>
                    <td><i class="fas fa-circle"></i> Projetos personalizados</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Serviço de revestimento</td>
                    <td><i class="fas fa-circle"></i> Solda especial</td>
                    <td><i class="fas fa-circle"></i> Usinagem</td>
                </tr>
            </tbody>
        </table>

        <table class="uk-table uk-table-divider uk-hidden@l">
            <tbody>
                <tr>
                    <td><i class="fas fa-circle"></i> Caldeiraria</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Metalização</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Projetos personalizados</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Serviço de revestimento</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Solda especial</td>
                </tr>
                <tr>
                    <td><i class="fas fa-circle"></i> Usinagem</td>
                </tr>
            </tbody>
        </table>

        <div class="spacing-lg uk-visible@l"></div>
        <div class="uk-grid-column-small uk-grid-row-large uk-child-width-1-3@s uk-text-center" id="servicos-de-aspersao" uk-grid>
            <div>
                <div><img class="uk-width-expand uk-visible@l" src="assets/images/services-icon.png" alt="services-image"></div>
            </div>
            <div>
                <div><img class="uk-width-expand uk-visible@l" src="assets/images/services-icon2.png" alt="services-image2"></div>
            </div>
            <div>
                <div><img class="uk-width-expand uk-visible@l" src="assets/images/services-icon3.png" alt="services-image3"></div>
            </div>
        </div>


        <div class="table-ligas">
            <table class="uk-table uk-table-small uk-table-divider uk-visible@l">
                <thead>
                    <tr>
                        <th colspan="2">Tipos de Ligas</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><i class="fas fa-circle"></i> Aço Carbono</td>
                        <td><i class="fas fa-circle"></i> Aço Inoxidável</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i> Bronze e Alumínio</td>
                        <td><i class="fas fa-circle"></i> Cobalto</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i>Cerâmicas</td>
                        <td><i class="fas fa-circle"></i> Metal Patente (Babbit)</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i> Níquel</td>
                        <td><i class="fas fa-circle"></i> Níquel com Carbonetos</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i> Polímeros</td>
                        <td><i class="fas fa-circle"></i> Zinco</td>
                    </tr>
                </tbody>
            </table>

            <table class="uk-table uk-table-small uk-table-divider uk-hidden@l">
                <thead>
                    <tr>
                        <th>Tipos de Liga:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><i class="fas fa-circle"></i> Aço Carbono</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i> Aço Inoxidável</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i> Bronze e Alumínio</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i> Cobalto</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i> Cerâmicas</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i> Metal Patente (Babbit)</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i> Níquel</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i> Níquel com Carbonetos</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i> Polímeros</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-circle"></i> Zinco</td>
                    </tr>
                </tbody>
            </table>

        </div>

        <div class="block-card">
            <h3 class="uk-text-center">Para mais informações, fale conosco.</h3>

            <div uk-slider="autoplay: true">
                <div class="uk-position-relative">
                    <div class="uk-slider-container uk-light">
                        <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-1@s uk-child-width-1-1@m">
                            <li>
                                <div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin-large-top" uk-grid>
                                    <div>
                                        <div class="block-orange">
                                            <p>SOLUÇÕES EM METALIZAÇÃO, SOLDA ESPECIAL, CALDEIRARIA E USINAGEM.</p>
                                        </div>
                                    </div>
                                    <div>
                                        <div><img class="uk-width-expand" src="assets/images/banner-footer-1.png" alt="banner-footer"></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin-large-top" uk-grid>
                                    <div>
                                        <div class="block-orange">
                                            <p>SERVIÇOS PARA PROJETOS INDUSTRIAIS DE PEQUENO, MÉDIO E GRANDE </p>
                                        </div>
                                    </div>
                                    <div>
                                        <div><img class="uk-width-expand" src="assets/images/banner-footer-2.png" alt="banner-footer"></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin-large-top" uk-grid>
                                    <div>
                                        <div class="block-orange">
                                            <p>AS MELHORES MARCAS DE CONSUMÍVEIS DE SOLDA DO MERCADO NACIONAL.</p>
                                        </div>
                                    </div>
                                    <div>
                                        <div><img class="uk-width-expand" src="assets/images/banner-footer-3.png" alt="banner-footer"></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin-large-top" uk-grid>
                                    <div>
                                        <div class="block-orange">
                                            <p>SOLUÇÕES EM ASPERSÃO. AGILIDADE, DURABILIDADE E QUALIDADE. </p>
                                        </div>
                                    </div>
                                    <div>
                                        <div><img class="uk-width-expand" src="assets/images/banner-footer-4.png" alt="banner-footer"></div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>