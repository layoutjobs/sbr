<section id="carousel-desktop">
    <div class="uk-visible@l" uk-slider="autoplay: true">
        <div class="uk-position-relative">
            <div class="uk-slider-container uk-light">
                <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-1@s uk-child-width-1-1@m">
                    <li>
                        <img src="assets/images/banner-topo1.png" alt="banner-1">
                    </li>
                    <li>
                        <img src="assets/images/banner-topo2.png" alt="banner-2">
                    </li>
                    <li>
                        <img src="assets/images/banner-topo3.png" alt="banner-3">
                    </li>
                    <li>
                        <img src="assets/images/banner-topo4.png" alt="banner-4">
                    </li>
                </ul>
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
            </div>
            <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
        </div>
    </div>
</section>

<section id="carousel-mobile" class="uk-hidden@l">
    <div uk-slider="center: true">

        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

            <ul class="uk-slider-items uk-child-width-1-1@s uk-grid">
                <li>
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-media-top">
                            <img class="uk-width-1-1" src="assets/images/b5.png" alt="img1">
                        </div>
                        <div class="uk-card-body">
                            <p>SOLUÇÕES EM METALIZAÇÃO, SOLDA ESPECIAL, CALDEIRARIA E USINAGEM.</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-media-top">
                            <img class="uk-width-1-1" src="assets/images/b3.png" alt="img2">
                        </div>
                        <div class="uk-card-body">
                            <p>AS MELHORES MARCAS DE CONSUMÍVEIS DE SOLDA DO MERCADO NACIONAL.</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-media-top">
                            <img class="uk-width-1-1" src="assets/images/b4.png" alt="img3">
                        </div>
                        <div class="uk-card-body">
                            <p>SERVIÇOS PARA PROJETOS INDUSTRIAIS DE PEQUENO, MÉDIO E GRANDE PORTE.</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-media-top">
                            <img class="uk-width-1-1" src="assets/images/b6.png" alt="img4">
                        </div>
                        <div class="uk-card-body">
                            <p>SOLUÇÕES EM ASPERSÃO. AGILIDADE, DURABILIDADE E QUALIDADE. </p>
                        </div>
                    </div>
                </li>
            </ul>

            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

        </div>

        <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
    </div>
</section>